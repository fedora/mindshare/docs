include::ROOT:partial$attributes.adoc[]

= Holding or Attending a Small Event
:page-aliases: ROOT:small-events.adoc

Anyone can hold or attend a small Fedora-related event, you don't have to be an Ambassador.
A small event is one where the budget is under $150.
The process is exactly the same as for our xref:ROOT:advocate.adoc[Advocate Program].
It is summarized below:

. Please open a Mindshare ticket in the link:{team_issue_tracker}[Mindshare Repository].
  You should use the **advocate** template from the `Type` drop down.
  Please complete the fields required.
+
When you are planning for an event you should think about using Fedora stickers and similar swag along with a budget of $150 or less.
If you need more budget, we encourage you to team up with a Fedora Ambassador have them sponsor the event and ask for a larger budget.
We are open to you suggesting uses for your budget that make the most sense for you, your area, and the event.

. Monitor your ticket for questions from the Committee.

. Voting usually starts very quickly.
  The ticket needs to get 2 +1s and no -1s in three days.
  When this happens, a committee member will update the ticket with an approved message.

. Open a **swag-shipment** request if you need swag for this event.
  Open the request in the link:https://gitlab.com/fedora/mindshare/event-requests[Event Requests repo] using the provided template.
  Please allow 2-3 weeks for shipping as swag is generally not stored in your country.
  Most of the delay is to allow time for customs processing in your country, not shipping.

. Hold your event!

. Write your event report
+
Fedora Events are often featured in the Mindshare newsletters and hence the event report holds a significant amount of importance.
The report is a general summary of an event which gives the project an idea of what's going on and reports on successes as well as lessons learned.
+
Reporting can be done in many ways.
Here's are a couple of commonly used methods for sharing the event report with the community:
+
* *Community Blog* (a.k.a. *CommBlog*): You can link:{COMMBLOG}/wp-login.php[sign into the Community Blog WordPress site] using your FAS account credentials.
  Once inside, you can create a new post where you can draft your experience.
  After you are done, follow the steps in the link:{COMMBLOG}/writing-community-blog-article/[guide on publishing articles on the CommBlog].
  Here's an example of a link:{COMMBLOG}/fedora-27-release-party-at-taipei/[Fedora 27 release party blog post].
+
* *Your own Blog* : If you have been maintaining your own blog, you can post it there as well.
  To extend the reach of audience, You may syndicate your blog to Fedora Planet.
  For more information link:{FWIKI}/Planet[check out this 'how to'].

. Submit a reimbursement request to the link:https://gitlab.com/fedora/mindshare/event-requests[Event Requests repo] using the provided template.
  Reimbursements are typically paid within 2 weeks.

. Comment on your issue in the link:{team_issue_tracker}[Mindshare repo] with a link to your event report.
