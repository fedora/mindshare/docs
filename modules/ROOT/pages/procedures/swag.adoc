= Creating and Ordering Swag

To make the production and creating of swag more efficient, the Mindshare Committee has empowered the link:https://docs.fedoraproject.org/en-US/council/fcaic/[Fedora Community Action and Impact Coordinator (FCAIC)] to produce and create swag as needed to keep our inventory full.
Swag items and use will be approved by Mindshare, in general, and then the FCAIC/others will fulfill needs for the various events based on their best judgement in concert with the event owner/coordinator.
The FCAIC will consult with Mindshare if there are questions.

Anyone can open a Mindshare ticket or contact the FCAIC to suggest swag items.
Event owners/coordinators can request specific swag when proposing their events.
No promises are made that specific swag items will be produced.

The goal of centralized swag production is to ensure high quality and reduced costs (through larger print runs).
We tend to use swag that is project themed and focused except for large events or special needs.

As an example, this process looks like this for the Fedora Booklets in link:https://pagure.io/mindshare/issue/105[Mindshare Ticket #105].

. Mindshare makes a decision about the use of the booklets in general and about Fedora providing them
. FCAIC/others arrange to have them printed and in-stock or on-demand for use
. Event Owners suggest when they will be useful at their events
. AMAZING EVENTS!
